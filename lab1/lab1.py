from OpenGL.GL import *
from OpenGL.GLU import *
from OpenGL.GLUT import *
import sys

a1, a2, a3, a4 = 0, 0, 0, 0

def pressKey(key, x, y):
    if key == GLUT_KEY_F1:
        glutPostRedisplay()
        global a1
        a1 += 2
    if key == GLUT_KEY_F2:
        global a2
        a2 += 2
    if key == GLUT_KEY_RIGHT:
        global a3
        a3 += 0.01
    if key == GLUT_KEY_LEFT:
        a3 -= 0.01
    if key == GLUT_KEY_UP:
        global a4
        a4 += 0.01
    if key == GLUT_KEY_DOWN:
        a4 -= 0.01
    glutPostRedisplay()

def display_tetrahedron():
    glPushMatrix()
    glTranslatef(-0.3, 0.0, 0.0)
    glScalef(0.2, 0.2, 0.2)
    global a1
    glRotatef(a1, 1, 0, 0)
    glutWireTetrahedron()
    glPopMatrix()

def display_sphere():
    glPushMatrix()
    glTranslatef(0.1, 0.0, 0.0)
    global a2
    glRotatef(a2, 0, 0, 1)
    glTranslatef(-0.1, 0.0, 0.0)
    glutWireSphere(0.1, 50, 50)
    glPopMatrix()

def display_cube():
    glPushMatrix()
    glTranslatef(-0.2, -0.4, 0.0)
    global a3, a4
    glTranslatef(a3, a4, 0)
    glRotated(30, 1, 1, 0)
    glutWireCube(0.2)
    glPopMatrix()

def display_torus():
    glPushMatrix()
    glTranslatef(0.2, -0.4, 0.0)
    glRotated(30, 1, 1, 0)
    glutWireTorus(0.05, 0.1, 50, 50)
    glPopMatrix()

def display():
    glClear(GL_COLOR_BUFFER_BIT)
    display_tetrahedron()
    display_sphere()
    display_cube()
    display_torus()
    glutSwapBuffers()

def main():
    glutInit(sys.argv)
    glutInitDisplayMode(GLUT_SINGLE | GLUT_RGB)
    glutInitWindowSize(2000, 1200)
    glutCreateWindow("Lab1")
    glutDisplayFunc(display)
    glutSpecialFunc(pressKey)
    glutMainLoop()

main()