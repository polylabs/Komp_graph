import pygame
from pygame.locals import *
from OpenGL.GL import *
from OpenGL.GLU import *
from OpenGL.GLUT import *
from particle import Particle
from emitter import Emitter

pygame.init()
glutInit()
display = (2000, 1200)
pygame.display.set_mode(display, DOUBLEBUF | OPENGL | OPENGLBLIT)
glShadeModel(GL_FLAT)
glEnable(GL_COLOR_MATERIAL)
glMatrixMode(GL_PROJECTION)
glLoadIdentity()
gluPerspective(30, (display[0] / display[1]), 0.1, 100.0)
glMatrixMode(GL_MODELVIEW)
glLoadIdentity()
gluLookAt(35, 0, 10, 0, 0, 0, 0, 0, 1) 

def create_particles(num_particles, surface_radius, trail_length):
    particles = []
    for _ in range(num_particles):
        particle = Particle(surface_radius, trail_length)
        particles.append(particle)
    return particles

last_time = pygame.time.get_ticks()
surface_radius = 5.0
trail_length = 6
particles = create_particles(300, surface_radius, trail_length)
emitter = Emitter(surface_radius)
glEnable(GL_BLEND)
glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA)
glEnable(GL_LIGHTING)
glEnable(GL_LIGHT0)
light_position = [10.0, 30.0,10.0, 1.0]
glLightfv(GL_LIGHT0, GL_POSITION, light_position)
light_ambient = [0.2, 0.2, 0.2, 1.0]
light_diffuse = [1.0, 1.0, 1.0, 1.0]
light_specular = [1.0, 1.0, 1.0, 1.0]
glLightfv(GL_LIGHT0, GL_AMBIENT, light_ambient)
glLightfv(GL_LIGHT0, GL_DIFFUSE, light_diffuse)
glLightfv(GL_LIGHT0, GL_SPECULAR, light_specular)

while True:
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            pygame.quit()
            quit()
        elif event.type == pygame.KEYDOWN:
            glRotatef(10, 0, 0, 1)
    current_time = pygame.time.get_ticks()
    print(pygame.time.get_ticks())
    dtime = (current_time - last_time) / 1000.0
    last_time = current_time
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT)

    for particle in particles:
        particle.update(dtime)
        particle.handle_collision()
        if particle.lifetime <= 0:
            particles.remove(particle)
            new_particle = Particle(surface_radius, trail_length)
            particles.append(new_particle)
    glClearColor(0.7, 0.7, 1.0, 1)
    glPushMatrix()
    glColor3f(0, 0, 0)
    glTranslatef(2,2,5)
    glutSolidCube(5)
    glPopMatrix()
    emitter.draw()
    for particle in particles:
        if particle.lifetime > 0:
            particle.draw()

    pygame.display.flip()
    pygame.time.wait(10)