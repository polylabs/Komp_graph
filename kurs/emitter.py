from OpenGL.GL import *
from OpenGL.GLU import *
from OpenGL.GLUT import *

class Emitter:
    def __init__(self, radius):
        self.radius = radius
        self.position = (0, 0, 0)

    def draw(self):
        glPushMatrix()
        glTranslatef(*self.position)
        glColor4f(0.2, 0.1, 0.7, 0.9) 
        glPushMatrix()
        glScalef(0.7, 0.7, 0.7)
        glutSolidCone(self.radius, 10, 100, 100)
        glPopMatrix()
        glPopMatrix()