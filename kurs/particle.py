from OpenGL.GL import *
from OpenGL.GLU import *
from OpenGL.GLUT import *
import math
import random

class Particle:
    def __init__(self, surface_radius, trail_length):
        theta = random.uniform(0, 2 * math.pi)
        self.position = [
            surface_radius * math.cos(theta),
            surface_radius * math.sin(theta),
            random.uniform(0, 10)
        ]
        self.velocity = [
            self.position[0] / surface_radius,
            self.position[1] / surface_radius,
            self.position[2] / surface_radius
        ]
        self.size = random.uniform(0.03, 0.035)
        self.color = (random.random(), random.random(), random.random())
        self.alpha = 1.0
        self.lifetime = random.uniform(2, 7)
        self.trail_length = trail_length
        self.trail = []
        self.acceleration = 1

    def handle_collision(self):
        if math.sqrt((self.position[0] - 0)**2 + (self.position[1] - 8)**2 + (self.position[2] - 6)**2) < 3:
            self.position[0] = -self.position[0]
            self.velocity[0] *= -1.5

    def update(self, dtime):
        for i in range(3):
            self.velocity[i] += self.acceleration * dtime 
            self.position[i] += self.velocity[i] * dtime
        self.alpha = self.lifetime / max(1.0, self.lifetime - dtime)
        self.lifetime -= dtime

        if len(self.trail) > self.trail_length:
            self.trail.pop(0)
        self.trail.append(self.position[:])

    def draw(self):
        glColor4f(self.color[0], self.color[1], self.color[2], self.alpha)
        particle_ambient = [0.1, 0.1, 0.1, 1.0]
        particle_diffuse = [0.8, 0.8, 0.8, 1.0]
        particle_specular = [0.0, 0.0, 0.0, 1.0]
        particle_shininess = 0.5

        glMaterialfv(GL_FRONT, GL_AMBIENT, particle_ambient)
        glMaterialfv(GL_FRONT, GL_DIFFUSE, particle_diffuse)
        glMaterialfv(GL_FRONT, GL_SPECULAR, particle_specular)
        glMaterialf(GL_FRONT, GL_SHININESS, particle_shininess)

        glBegin(GL_QUADS)
        size = self.size
        x, y, z = self.position
        glVertex3f(x - size, y - size, z)
        glVertex3f(x + size, y - size, z)
        glVertex3f(x + size, y + size, z)
        glVertex3f(x - size, y + size, z)
        glEnd()

        trail_color = tuple(c * 0.8 for c in self.color)
        glColor4f(trail_color[0], trail_color[1], trail_color[2], self.alpha)
        glBegin(GL_QUADS)
        for point in self.trail:
            x, y, z = point
            size = self.size
            glVertex3f(x - size, y - size, z)
            glVertex3f(x + size, y - size, z)
            glVertex3f(x + size, y + size, z)
            glVertex3f(x - size, y + size, z)
        glEnd()