from OpenGL.GL import *
from OpenGL.GLU import *
from OpenGL.GLUT import *
import sys

from PIL import Image
global x_axis, y_axis, img, img_data, r, g, b
x_axis, y_axis = 0, 0
r, g, b = 1, 0, 0

def pressKey(key, x, y):
    if key == GLUT_KEY_RIGHT:
        global x_axis
        x_axis += 1
    if key == GLUT_KEY_LEFT:
        x_axis -= 1
    if key == GLUT_KEY_UP:
        global y_axis
        y_axis += 1
    if key == GLUT_KEY_DOWN:
        y_axis -=1
    glutPostRedisplay()

def rgbKey(key, x, y):
    if key == b'r':
        global r
        r += 1
    if key == b'g':
        global g
        g += 1
    if key == b'b':
        global b
        b += 1
    glutPostRedisplay()

def display_sphere():
    glPushMatrix()
    glEnable(GL_TEXTURE_2D)
    glEnable(GL_TEXTURE_GEN_S)
    glEnable(GL_TEXTURE_GEN_T)
    glTranslatef(0.1, 0, 0.0)
    glPixelStorei(GL_UNPACK_ALIGNMENT, 1)
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, img.width, img.height, 0, GL_RGBA, GL_UNSIGNED_BYTE, img_data)
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST)
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST)
    glTexGeni(GL_S, GL_TEXTURE_GEN_MODE, GL_OBJECT_LINEAR)
    glTexGeni(GL_T, GL_TEXTURE_GEN_MODE, GL_OBJECT_LINEAR)
    glutSolidSphere(0.1, 50, 50)
    glDisable(GL_TEXTURE_2D)
    glDisable(GL_TEXTURE_GEN_S)
    glDisable(GL_TEXTURE_GEN_T)
    glPopMatrix()

def display_cube():
    glPushMatrix()
    glBlendFunc(GL_SRC_ALPHA, GL_SRC_COLOR)
    glEnable(GL_BLEND)
    glTranslatef(-0.2, 0, 0.0)
    glRotated(30, 1, 1, 0)
    glTranslatef(0, 0, 0)
    glutSolidCube(0.2)
    glPopMatrix()

def display_torus():
    glPushMatrix()
    glTranslatef(0.4, 0, 0.0)
    glRotated(30, 1, 1, 0)
    glMaterialf(GL_FRONT_AND_BACK, GL_SHININESS, 127)
    glMaterialfv(GL_FRONT_AND_BACK, GL_SPECULAR, (1, 1, 1, 1))
    glutSolidTorus(0.05, 0.1, 50, 50)
    glMaterialfv(GL_FRONT_AND_BACK, GL_SPECULAR, (10, 0, 0, 0))
    glTranslatef(-0.5, 0, 0)
    glPopMatrix()

def display():
    glClear(GL_COLOR_BUFFER_BIT)

    global x_axis, y_axis, img, img_data, r, g, b

    img = Image.open("texture.jpg")
    img_data = img.convert("RGBA").tobytes()

    glClearColor(0.5, 0.5, 0.5, 1.0)
    glLightModelfv(GL_LIGHT_MODEL_AMBIENT, (r, g, b, 1))
    glEnable(GL_LIGHTING)
    glEnable(GL_LIGHT0)
    glLightfv(GL_LIGHT0, GL_POSITION, (x_axis, y_axis, 5))
    glLightfv(GL_LIGHT0, GL_DIFFUSE, (r, g, b))
    glLightfv(GL_LIGHT0, GL_SPECULAR, (r, g, b, 0.5, 0.5))
    glLightf(GL_LIGHT0, GL_LINEAR_ATTENUATION, 1)
    
    display_cube()
    display_sphere()
    display_torus()

    glutSwapBuffers()

def main():
    glutInit(sys.argv)
    glutInitDisplayMode(GLUT_SINGLE | GLUT_RGB)
    glutInitWindowSize(2000, 1200)
    glutCreateWindow("Lab1")
    glutDisplayFunc(display)
    glutSpecialFunc(pressKey)
    glutKeyboardFunc(rgbKey)
    glutMainLoop()

main()