from OpenGL.GL import *
from OpenGL.GLU import *
from OpenGL.GLUT import *
import math
from PIL import Image

yCamAngle = 30
time = 0 # Счетчик времени (для моделирования движения).

SIDE = 0.10514622242387393  # Сторона треугольника(грани икосаэдра)
Z_DIFF = (SIDE / 2) * (1/2)  # Половина стороны на синус 30 градусов.
X_DIFF = (SIDE / 2) * (math.sqrt(3) / 2) # Половина стороны на синус 60 градусов.
ANGLE = 20.9051575  # Половина угла вращения.

def loadTexture(filename):
    image = Image.open(filename)
    width, height = image.size
    image_data = image.convert("RGBA").tobytes()
    texture_id = glGenTextures(1) 
    glPixelStorei(GL_UNPACK_ALIGNMENT, 1)
    glBindTexture(GL_TEXTURE_2D, texture_id)
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST)
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, width, height, 0, GL_RGBA, GL_UNSIGNED_BYTE, image_data)
    return texture_id

def init():
    global ICOSAHEDRON_TEXTURE
    ICOSAHEDRON_TEXTURE = loadTexture("lab3/icosahedron.jpg")
    glEnable(GL_BLEND)
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA)
    glEnable(GL_CULL_FACE)
    glEnable(GL_LIGHTING)
    glEnable(GL_LIGHT0)
    glClearColor(0.7, 0.7, 1.0, 1)

def pressKey(key, x, y):
    global yCamAngle
    if key == GLUT_KEY_RIGHT:
        yCamAngle += 5
    if key == GLUT_KEY_LEFT:
        yCamAngle -= 5

def rotate(xr, yr, zr):
    glRotatef(xr, 1.0, 0.0, 0.0)
    glRotatef(yr, 0.0, 1.0, 0.0)
    glRotatef(zr, 0.0, 0.0, 1.0)

def movePart(lBorder, rBorder, moveNumber, initialAngle, dx, dy, rz):
    if moveNumber >= lBorder and moveNumber < rBorder:
        glPushMatrix()
        glTranslatef(dx, 0.0, dy)
        glRotatef(rz, 0.0, 1.0, 0.0)
        rotate(
            0.0,
            0.0,
            initialAngle + (moveNumber % 100) * -ANGLE / 100
        )
        drawIcosahedron(ICOSAHEDRON_TEXTURE)
        glPopMatrix()

def action():
    global X_DIFF
    global Z_DIFF
    global time

    move = time % 2800

    glPushMatrix()

    movePart(0, 100, move, 0.0, 0.0, 0.0, 0.0)
    movePart(100, 200, move, -ANGLE, 0.0, 0.0, 0.0)
    movePart(200, 300, move, 0.0, X_DIFF, -Z_DIFF, 60)
    movePart(300, 400, move, -ANGLE, X_DIFF, -Z_DIFF, 60)
    movePart(400, 500, move, 0.0, X_DIFF, -3 * Z_DIFF, 120)
    movePart(500, 600, move, -ANGLE, X_DIFF, -3 * Z_DIFF, 120)
    movePart(600, 700, move, 0.0, X_DIFF, -5* Z_DIFF, 60)
    movePart(700, 800, move, -ANGLE, X_DIFF, -5*Z_DIFF, 60)
    movePart(800, 900, move, 0.0, X_DIFF, -7*Z_DIFF, 120)
    movePart(900, 1000, move, -ANGLE, X_DIFF, -7*Z_DIFF, 120)
    movePart(1000, 1100, move, 0.0, X_DIFF, -9* Z_DIFF, 60)
    movePart(1100, 1200, move, -ANGLE, X_DIFF, -9*Z_DIFF, 60)
    movePart(1200, 1300, move, 0.0, X_DIFF, -11*Z_DIFF, 120)
    movePart(1300, 1400, move, -ANGLE, X_DIFF, -11*Z_DIFF, 120)
    movePart(1400, 1500, move, 0.0, 0, -12*Z_DIFF, 180)
    movePart(1500, 1600, move, -ANGLE, 0, -12*Z_DIFF, 180)
    movePart(1600, 1700, move, 0.0, -X_DIFF, -11 * Z_DIFF, 240)
    movePart(1700, 1800, move, -ANGLE, -X_DIFF, -11 * Z_DIFF, 240)
    movePart(1800, 1900, move, 0.0, -X_DIFF, -9* Z_DIFF, 300)
    movePart(1900, 2000, move, -ANGLE, -X_DIFF, -9*Z_DIFF, 300)
    movePart(2000, 2100, move, 0.0, -X_DIFF, -7 * Z_DIFF, 240)
    movePart(2100, 2200, move, -ANGLE, -X_DIFF, -7 * Z_DIFF, 240)
    movePart(2200, 2300, move, 0.0, -X_DIFF, -5* Z_DIFF, 300)
    movePart(2300, 2400, move, -ANGLE, -X_DIFF, -5*Z_DIFF, 300)
    movePart(2400, 2500, move, 0.0, -X_DIFF, -3* Z_DIFF, 240)
    movePart(2500, 2600, move, -ANGLE, -X_DIFF, -3*Z_DIFF, 240)
    movePart(2600, 2700, move, 0.0, -X_DIFF, -1* Z_DIFF, 300)
    movePart(2700, 2800, move, -ANGLE, -X_DIFF, -1*Z_DIFF, 300)

    glPopMatrix()

def drawCube():
    glPushMatrix()
    glMaterialfv(GL_FRONT, GL_SHININESS, 0.0)
    glMaterialfv(GL_FRONT, GL_AMBIENT_AND_DIFFUSE, (0.6, 0.6, 0.6, 1))
    glMaterialfv(GL_FRONT, GL_SPECULAR, (0.8, 0.8, 0.8, 1))
    glEnable(GL_TEXTURE_2D)
    cubeTexture = loadTexture("lab3/cube.jpg")
    glBindTexture(GL_TEXTURE_2D, cubeTexture)
    glBegin(GL_QUADS)

    glNormal3fv((0.0, 1.0, 0.0))
    glTexCoord2f(0.0, 0.0)
    glVertex3fv((0.5, 0.0, 0.5))
    glTexCoord2f(1.0, 0.0)
    glVertex3fv((0.5, 0.0, -0.5))
    glTexCoord2f(1.0, 1.0)
    glVertex3fv((-0.5, 0.0, -0.5))
    glTexCoord2f(0.0, 1.0)
    glVertex3fv((-0.5, 0.0, 0.5))

    glEnd()

    glDisable(GL_TEXTURE_2D)
    glPopMatrix()

def drawIcosahedron(texture):
    glPushMatrix()
    # Поворачиваем икосаэдр так, чтобы оси выходили из середины ребра основания.
    glTranslatef(-0.030353099910, 0.079465, 0.0)
    glRotate(-11, 0.0, 0.0, 1.0)
    glMaterialfv(GL_FRONT, GL_SHININESS, 0.0)
    glMaterialfv(GL_FRONT, GL_AMBIENT_AND_DIFFUSE, (0.6, 0.6, 0.6, 1))
    glMaterialfv(GL_FRONT, GL_SPECULAR, (0.8, 0.8, 0.8, 1))
    glScalef(0.1, 0.1, 0.1)
    glEnable(GL_TEXTURE_2D)
    glBindTexture(GL_TEXTURE_2D, texture)
    glutSolidIcosahedron()
    glDisable(GL_TEXTURE_2D)
    glPopMatrix()

def setLight():
    glPushMatrix()
    glLightfv(GL_LIGHT0, GL_POSITION, (1.0, 1.0, 1.0, 1.0))
    glLightfv(GL_LIGHT0, GL_AMBIENT, (0.4, 0.4, 0.4))
    glLightfv(GL_LIGHT0, GL_DIFFUSE, (0.4, 0.4, 0.4))
    glLightfv(GL_LIGHT0, GL_SPECULAR, (0.4, 0.4, 0.4))
    glPopMatrix()

def draw():
    global time
    glLoadIdentity()
    rotate(20, yCamAngle, 0.0)
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT) 
    setLight()
    glPushMatrix()
    drawCube()
    glPopMatrix()
    action()
    timeDiff = 0
    t = time % 100
    if time // 100 % 2 == 0:
        timeDiff = 7 * t / 100 + 0.1
    else:
        timeDiff = 7
    time += timeDiff
    glutSwapBuffers() 
    glutPostRedisplay() 


def main():
    glutInit()
    glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGB)
    glutInitWindowSize(1000, 1000)
    glutCreateWindow("Lab3")
    glutDisplayFunc(draw)
    glutSpecialFunc(pressKey)
    init()
    glutMainLoop()

main()
